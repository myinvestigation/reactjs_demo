import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import logo from './logo.svg';
import $ from 'jquery'
// import './App.css';
import Users from './components/User/ListUser';
import UserEdit from './components/User/edit';
import Login from './components/User/login';

class App extends Component {

  constructor(props) {
      super(props);
      this.state = {
        is_login : false,
        user:{}
      }
      
      this.logout = this.logout.bind(this);
      this.check_login();
   };

   check_login(){
      var this_tmp = this;
      $.ajax( {        
        url: "http://localhost:3000/api/v1/users/info_login", 
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          if (rs.success){
            console.log("check_login: success");
            this_tmp.setState({is_login: true, user:rs.user})
          }
          else{
            console.log("check_login: " + rs.message)
          }
          console.log(rs);
        },
        error: function(e){
          console.log("check_login: Loi")
        }
      });

   }

  logout(e){
      $.ajax( {        
        url: "http://localhost:3000/api/v1/users/logout", 
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        success: function(rs){
          if (rs.success){
            alert("Dang xuat thanh cong")
          }
          else{
            alert("That bai: " + rs.message)
          }
          console.log(rs);
        },
        error: function(e){
          alert("Loi")
        }
      });
  }
  render() {
    return (
      <div className="App">
        

        <Router>
            <div>
               
               <ul>
                  <li><Link to={'/users'}>List users</Link></li>
                  
                  {
                    this.state.is_login?(
                      <li>Hello {this.state.user.username}<button onClick={this.logout}>Logout</button></li>
                    ):(
                      <li><Link to={'/login'}>Login</Link> | <Link to={'/add'}>Register</Link></li>
                    )
                  }
               </ul>
               <hr />
               
               <Switch>
                  <Route exact path='/users' component={Users} />
                  <Route exact path='/edit/:id' component={UserEdit} />
                  <Route exact path='/add' component={UserEdit} />
                  <Route exact path='/login' component={Login} />
               </Switch>
            </div>
         </Router>
      </div>

    );
  }
}

export default App;
